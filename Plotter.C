{

  // ###############    PARAMETERS   ###############

  double totcutlow=0; // Low cut in TOT
  double totcuthigh=5; // High cut in TOT
  double LGADcutlow=70; // Low cut in LGAD amplitude
  double LGADcuthigh=120; // High cut in LGAD amplitude
  int DoPeakTW=1; // If 1 does the TW correction around the peak. If 0 it uses the mean values
  double Wpeak=0.2; // Width around the peak for the time walk correction
  int DoBorder=2; // If 1 inludes the events in which the other adjacent pixels fire (with dedicated TW correction). If 0 they are excluded from the analysis.
  int bintw=50;
  int bintot=200;
  int bintof=120;
  int doslices=1; // If 1 does the gaussian fit of the peak for each tot slice
  double totslice=0.05;
  int slicecontent=100;
  string filename;

  // ###############  SCAN SELECTION  ###############

  //  const Int_t nscan=4; // Number of runs in the scan
  // Int_t ScanList[nscan]={16,17,19,20}; // List of runs to analyse
  // Double_t Thr[nscan]={966,985,951,988};
  // Double_t HV[nscan]={180,180,180,180};

  int scanID=4;

  Int_t nscan=4;
  const Int_t max_nscan=10; // Number of runs in the scan
  Int_t ScanList[max_nscan]; // List of runs to analyse
  Double_t Thr[max_nscan];
  Double_t HV[max_nscan];

  if(scanID==1){
    filename="OutputFile.root";
    nscan=4; // Number of runs in the scan
    ScanList[0]=16;
    ScanList[1]=17;
    ScanList[2]=19;
    ScanList[3]=20; // List of runs to analyse
    Thr[0]=6250*(966-920)/185.;
    Thr[1]=6250*(985-920)/185.;
    Thr[2]=6250*(951-920)/185.;
    Thr[3]=6250*(988-920)/185.;
    for(int i=0;i<nscan;i++){
      HV[i]=180;
    }
  }
  else if(scanID==2){
    filename="OutputFile.root";
    nscan=3; // Number of runs in the scan
    ScanList[0]=21;
    ScanList[1]=22;
    ScanList[2]=23;
    Thr[0]=6250*(988-920)/185.;
    Thr[1]=6250*(968-920)/185.;
    Thr[2]=6250*(952-920)/185.;
    for(int i=0;i<nscan;i++){
      HV[i]=140;
    }
  }
  else if(scanID==3){
    filename="OutputFile_TOF_WP1.root";
    nscan=3; // Number of runs in the scan
    ScanList[0]=136;
    ScanList[1]=135;
    ScanList[2]=168;
    Thr[0]=6250*(965-920)/185.;
    Thr[1]=6250*(987-920)/185.;
    Thr[2]=6250*(951-920)/185.;
    for(int i=0;i<nscan;i++){
      HV[i]=160;
    }
  }
  else if(scanID==4){
    filename="OutputFile_TOF_WP1s.root";
    nscan=1; // Number of runs in the scan
//    ScanList[0]=173;
//    ScanList[0]=172;
    ScanList[0]=174;
//    ScanList[0]=172;
    //ScanList[0]=170;
//    Thr[0]=6250*(980-940)/265.;
//    Thr[0]=6250*(995-956)/265.;
    Thr[0]=6250*(1012-956)/265.;
    HV[0]=160;
//    HV[1]=160;
    totcutlow=0;
    totcuthigh=5;
  }

  //gStyle->SetLineScalePS(1.0);

  TCanvas *Ccheck = new TCanvas("Ccheck", "Ccheck",20,71,3095,1680); //TOT, TW (con Chi2/NDF), Chi2-Fit vs TOT
  Ccheck->Divide(2,2);

  //  Int_t ScanList[nscan]={21,22,23}; // List of runs to analyse
  // Double_t Thr[nscan]={988,968,952};
  // Double_t HV[nscan]={180,180,180,180};

  Double_t ThrErr[max_nscan];
  TCut ScanCut[max_nscan];
  Double_t Sigma[max_nscan];
  Double_t SigmaErr[max_nscan];

  //cout << TH1D::EStatusBits::kNoStats << endl;

  // ###############      BEGIN      ###############

  TFile fIN(filename.c_str(),"read");
  TTree *EventData=(TTree*)fIN.Get("EventData");

  TCanvas c1("c1", "c1",20,71,2095,1280);
  c1.cd();
  c1.Range(-0.625,-22.44375,0.625,201.9938);

  TF1 fp[6];

  TF1 fp0("fp0","pol0(0)",-0.1,200+0.1);
  fp0.Copy(fp[0]);
  TF1 fp1("fp1","pol1(0)",-0.1,200+0.1);
  fp1.Copy(fp[1]);
  TF1 fp2("fp2","pol2(0)",-0.1,200+0.1);
  fp2.Copy(fp[2]);
  TF1 fp3("fp3","pol3(0)",-0.1,200+0.1);
  fp3.Copy(fp[3]);
  TF1 fp4("fp4","pol4(0)",-0.1,200+0.1);
  fp4.Copy(fp[4]);
  TF1 fp5("fp5","pol5(0)",-0.1,200+0.1);
  fp5.Copy(fp[5]);

  TF1 ftof("ftof","gaus(0)",-1,1);
  TF1 fgaus("fgaus","gaus(0)",-20,-8);

  //cout << "check1" << endl;
  ostringstream cmd;

  double TWhex[6];
  double TWhexSmall[6];
  double TWLGAD[6];

  //EventData->Draw("time_2thr3-time_2thr4>>htest");
  EventData->Draw("time_2thr3-time_2thr4>>htest(100,-30,-5)");
  double tofcutlow=htest->GetBinCenter(htest->GetMaximumBin())-2.;
  double tofcuthigh=htest->GetBinCenter(htest->GetMaximumBin())+2.;

  cout << "TOF range: " << tofcutlow << " to " << tofcuthigh << endl;

  TH1D *hTOT_new;
  TH1D *hTOF_new;
  TH2D *hTOF2D_new;

  TH1D *hTOT = new TH1D("hTOT","hTOT",bintot,0,10);
  TProfile *htwhex = new TProfile("htwhex","htwhex",bintw,0,totcuthigh,tofcutlow,tofcuthigh);
  TProfile *htwhex_border = new TProfile("htwhex_border","htwhex_border",bintw,0,totcuthigh,tofcutlow,tofcuthigh);
  TProfile *htwhex_border_save_before;
  TProfile *htwhex_border_save_after;
  TProfile *htwLGAD = new TProfile("htwLGAD","htwLGAD",100,20,170,-1,-1);
  TH1D *hTOF = new TH1D("hTOF","hTOF",bintof,-0.7,0.7);
  TH2D *hTOF2D = new TH2D("hTOF2D","hTOF2D",3000,0,3000,bintof,-0.7,0.7);
  TH1D *hgaus = new TH1D("hgaus","hgaus",50,tofcutlow,tofcuthigh);
  TGraph *gtest = new TGraph();
  TGraphErrors *g = new TGraphErrors();

  TPaveStats *ptstats_TOT = new TPaveStats(0.65,0.7,0.8963211,0.8955343,"brNDC");
  TText *ptstats_TOT_LaTex = new TText;
  ptstats_TOT->SetName("stats");
  ptstats_TOT->SetBorderSize(1);
  ptstats_TOT->SetFillColor(0);
  ptstats_TOT->SetTextAlign(12);
  ptstats_TOT->SetTextFont(42);

  TPaveStats *pinfo_TOT = new TPaveStats(0.65,0.15,0.89,0.35,"brNDC");
  TText *pinfo_TOT_LaTex = new TText;
  pinfo_TOT->SetName("info");
  pinfo_TOT->SetBorderSize(1);
  pinfo_TOT->SetFillColor(0);
  pinfo_TOT->SetTextAlign(12);
  pinfo_TOT->SetTextFont(42);

  TPaveStats *ptstats_hex = new TPaveStats(0.65,0.7,0.8963211,0.8955343,"brNDC");
  TText *ptstats_hex_LaTex = new TText;
  ptstats_hex->SetName("stats");
  ptstats_hex->SetBorderSize(1);
  ptstats_hex->SetFillColor(0);
  ptstats_hex->SetTextAlign(12);
  ptstats_hex->SetTextFont(42);

  TPaveStats *pinfo_hex = new TPaveStats(0.15,0.15,0.4,0.35,"brNDC");
  TText *pinfo_hex_LaTex = new TText;
  pinfo_hex->SetName("info");
  pinfo_hex->SetBorderSize(1);
  pinfo_hex->SetFillColor(0);
  pinfo_hex->SetTextAlign(12);
  pinfo_hex->SetTextFont(42);

  TPaveStats *ptstats_LGAD = new TPaveStats(0.65,0.7,0.8963211,0.8955343,"brNDC");
  TText *ptstats_LGAD_LaTex = new TText;
  ptstats_LGAD->SetName("stats");
  ptstats_LGAD->SetBorderSize(1);
  ptstats_LGAD->SetFillColor(0);
  ptstats_LGAD->SetTextAlign(12);
  ptstats_LGAD->SetTextFont(42);

  TPaveStats *ptstats_TOF = new TPaveStats(0.59,0.55,0.8963211,0.8955343,"brNDC");
  TText *ptstats_TOF_LaTex = new TText;
  ptstats_TOF->SetName("stats");
  ptstats_TOF->SetBorderSize(1);
  ptstats_TOF->SetFillColor(0);
  ptstats_TOF->SetTextAlign(12);
  ptstats_TOF->SetTextFont(42);

  TPaveStats *pinfo_TOF = new TPaveStats(0.15,0.65,0.4,0.85,"brNDC");
  TText *pinfo_TOF_LaTex = new TText;
  pinfo_TOF->SetName("info");
  pinfo_TOF->SetBorderSize(1);
  pinfo_TOF->SetFillColor(0);
  pinfo_TOF->SetTextAlign(12);
  pinfo_TOF->SetTextFont(42);

//fdibello - saving histo for cosmetics

//  TFile *Output = new TFile("Francesco_plots.root","RECREATE");

  for(int i=0;i<nscan; i++){

    // ###############       TOT       ###############

    //Ccheck->Divide(2,2);

    ScanCut[i]=Form("nrun==%d && max4<%f && max4>%f && abs(time_2thr3-time_2thr4+9.25)<100 && trail_2thr3-time_2thr3>%f && trail_2thr3-time_2thr3<%f",ScanList[i],LGADcuthigh,LGADcutlow,totcutlow,totcuthigh);
    EventData->Draw("trail_2thr3-time_2thr3>>hTOT",ScanCut[i],"");
    hTOT->SetTitle("Time Over Threshold; TOT [ns]; Entries");
    hTOT->SetLineWidth(3);
    hTOT->GetXaxis()->SetRangeUser(0,totcuthigh+1);
    hTOT->GetXaxis()->SetLabelFont(42);
    hTOT->GetXaxis()->SetLabelSize(0.05);
    hTOT->GetXaxis()->SetTitleSize(0.05);
    hTOT->GetXaxis()->SetTitleOffset(0.94);
    hTOT->GetXaxis()->SetTitleFont(42);
    hTOT->SetStats(0);
    ptstats_TOT->Clear();
    ptstats_TOT_LaTex = ptstats_TOT->AddText(Form("Entries = %5.0f   ",hTOT->GetEntries()));
    ptstats_TOT->SetOptStat(0000);
    hTOT->GetListOfFunctions()->Add(ptstats_TOT);
    ptstats_TOT->SetParent(hTOT);
    pinfo_TOT->Clear();
    pinfo_TOT_LaTex = pinfo_TOT->AddText(Form("HV = %3.0f V ",HV[i]));
    pinfo_TOT_LaTex = pinfo_TOT->AddText(Form("Threshold = %3.0f e^{_ } ",Thr[i]));
    pinfo_TOT->Draw();
    pinfo_TOT->SetOptStat(0000);
    hTOT->GetListOfFunctions()->Add(pinfo_TOT);
    pinfo_TOT->SetParent(hTOT);
    c1.Update();
    Ccheck->cd(1);
    hTOT->Draw();
    Ccheck->Update();
    gPad->Update();
    c1.cd();
    cmd.clear();
    cmd.str("");
    cmd << "Run" << ScanList[i] << "_TOT.png";
    c1.SaveAs(cmd.str().c_str());
 //   hTOT->Write();
    //sleep(1);
    //getchar();

    // ############### TIME RESOLUTION ###############

    ScanCut[i]=Form("nrun==%d && max4<%f && max4>%f && abs(time_2thr3-time_2thr4+9.25)<100 && trail_2thr3-time_2thr3>%f && trail_2thr3-time_2thr3<%f",ScanList[i],LGADcuthigh,LGADcutlow,totcutlow,totcuthigh);

    // Time Walk correction Hexachip for central events

    if(doslices==0){
      EventData->Draw("time_2thr3-time_2thr4:trail_2thr3-time_2thr3>>htwhex",ScanCut[i]+"max1<5","prof");
      htwhex->GetXaxis()->SetRangeUser(totcutlow,totcuthigh);
      htwhex->GetYaxis()->SetRangeUser(tofcutlow,tofcuthigh);
      htwhex->Fit(&fp[0],"QN","",totcutlow,totcuthigh);
      for(int j=1;j<6;j++){
	fp[j].SetParameter(j,0);
	for(int k=0;k<j;k++){
	  fp[j].SetParameter(k,fp[j-1].GetParameter(k));
	}
	htwhex->Fit(&fp[j],"NQ","",totcutlow,totcuthigh);
      }

      for(int j=0;j<6;j++){
	TWhex[j]=fp[5].GetParameter(j);
      }
      EventData->Draw("time_2thr3-time_2thr4:trail_2thr3-time_2thr3",ScanCut[i],"same");
      htwhex->Reset();

      // Time Walk correction Hexachip for border events
      EventData->Draw("time_2thr3-time_2thr4:trail_2thr3-time_2thr3>>htwhex",ScanCut[i]+"max1>5","prof");
      htwhex->GetXaxis()->SetRangeUser(totcutlow,totcuthigh);
      htwhex->GetYaxis()->SetRangeUser(tofcutlow,tofcuthigh);
      htwhex->Fit(&fp[0],"QN","",totcutlow,totcuthigh);
      for(int j=1;j<6;j++){
	fp[j].SetParameter(j,0);
	for(int k=0;k<j;k++){
	  fp[j].SetParameter(k,fp[j-1].GetParameter(k));
	}
	htwhex->Fit(&fp[j],"NQ","",totcutlow,totcuthigh);
      }

      for(int j=0;j<6;j++){
	TWhexSmall[j]=fp[5].GetParameter(j);
      }
      EventData->Draw("time_2thr3-time_2thr4:trail_2thr3-time_2thr3",ScanCut[i],"same");
      htwhex->Reset();

      // Fit TW around peak
      if(DoPeakTW==1){
	htwhex->Reset();
	EventData->Draw("time_2thr3-time_2thr4:trail_2thr3-time_2thr3>>htwhex",ScanCut[i]+Form("abs(time_2thr3-time_2thr4-(%f+%f*pow((trail_2thr3-time_2thr3),1)+%f*pow((trail_2thr3-time_2thr3),2)+%f*pow((trail_2thr3-time_2thr3),3)+%f*pow((trail_2thr3-time_2thr3),4)+%f*pow((trail_2thr3-time_2thr3),5)))<%f",TWhex[0],TWhex[1],TWhex[2],TWhex[3],TWhex[4],TWhex[5],Wpeak),"prof");
	htwhex->GetXaxis()->SetRangeUser(totcutlow,totcuthigh);
	htwhex->GetYaxis()->SetRangeUser(tofcutlow,tofcuthigh);
	htwhex->Fit(&fp[0],"QN","",totcutlow,totcuthigh);
	for(int j=1;j<6;j++){
	  fp[j].SetParameter(j,0);
	  for(int k=0;k<j;k++){
	    fp[j].SetParameter(k,fp[j-1].GetParameter(k));
	  }
	  htwhex->Fit(&fp[j],"NQ","",totcutlow,totcuthigh);
	}
	EventData->Draw("time_2thr3-time_2thr4:trail_2thr3-time_2thr3",ScanCut[i]+Form("abs(time_2thr3-time_2thr4-(%f+%f*pow((trail_2thr3-time_2thr3),1)+%f*pow((trail_2thr3-time_2thr3),2)+%f*pow((trail_2thr3-time_2thr3),3)+%f*pow((trail_2thr3-time_2thr3),4)+%f*pow((trail_2thr3-time_2thr3),5)))<%f",TWhex[0],TWhex[1],TWhex[2],TWhex[3],TWhex[4],TWhex[5],Wpeak),"same");
	for(int j=0;j<6;j++){
	  TWhex[j]=fp[5].GetParameter(j);
	}
      }
      else{
	htwhex->Reset();
	EventData->Draw("time_2thr3-time_2thr4:trail_2thr3-time_2thr3>>htwhex",ScanCut[i],"prof");
	htwhex->GetXaxis()->SetRangeUser(totcutlow,totcuthigh);
	htwhex->GetYaxis()->SetRangeUser(tofcutlow,tofcuthigh);
	htwhex->Fit(&fp[0],"QN","",totcutlow,totcuthigh);
	for(int j=1;j<6;j++){
	  fp[j].SetParameter(j,0);
	  for(int k=0;k<j;k++){
	    fp[j].SetParameter(k,fp[j-1].GetParameter(k));
	  }
	  htwhex->Fit(&fp[j],"NQ","",totcutlow,totcuthigh);
	}
	EventData->Draw("time_2thr3-time_2thr4:trail_2thr3-time_2thr3",ScanCut[i],"same");
	for(int j=0;j<6;j++){
	  TWhex[j]=fp[5].GetParameter(j);
	}
      }
    }
    else if(doslices==1){

      // Time Walk correction Hexachip for border events (not in slices)
      htwhex_border->Reset();
      EventData->Draw("time_2thr3-time_2thr4:trail_2thr3-time_2thr3>>htwhex_border",ScanCut[i]+"max1>5","prof");
      htwhex_border->GetXaxis()->SetRangeUser(totcutlow,totcuthigh);
      htwhex_border->GetYaxis()->SetRangeUser(tofcutlow,tofcuthigh);
      htwhex_border->Fit(&fp[0],"QN","",totcutlow,totcuthigh);
      for(int j=1;j<6;j++){
	fp[j].SetParameter(j,0);
	for(int k=0;k<j;k++){
	  fp[j].SetParameter(k,fp[j-1].GetParameter(k));
	}
	htwhex_border->Fit(&fp[j],"NQ","",totcutlow,totcuthigh);
      }

      for(int j=0;j<6;j++){
	TWhexSmall[j]=fp[5].GetParameter(j);
      }

      htwhex_border_save_before = htwhex_border;
      //htwhex_border->Print("TOW_fdibello.png");
      htwhex_border->Reset();

      EventData->Draw("time_2thr3-time_2thr4:trail_2thr3-time_2thr3>>htwhex_border",ScanCut[i]+"max1>5"+Form("abs(time_2thr3-time_2thr4-(%f+%f*pow((trail_2thr3-time_2thr3),1)+%f*pow((trail_2thr3-time_2thr3),2)+%f*pow((trail_2thr3-time_2thr3),3)+%f*pow((trail_2thr3-time_2thr3),4)+%f*pow((trail_2thr3-time_2thr3),5)))<%f",TWhexSmall[0],TWhexSmall[1],TWhexSmall[2],TWhexSmall[3],TWhexSmall[4],TWhexSmall[5],Wpeak),"prof");
      c1.Update();
      //getchar();
      htwhex_border->GetXaxis()->SetRangeUser(totcutlow,totcuthigh);
      htwhex_border->GetYaxis()->SetRangeUser(tofcutlow,tofcuthigh);
      htwhex_border->Fit(&fp[0],"QN","",totcutlow,totcuthigh);
      for(int j=1;j<6;j++){
	fp[j].SetParameter(j,0);
	for(int k=0;k<j;k++){
	  fp[j].SetParameter(k,fp[j-1].GetParameter(k));
	}
	htwhex_border->Fit(&fp[j],"NQ","",totcutlow,totcuthigh);
      }

      for(int j=0;j<6;j++){
	TWhexSmall[j]=fp[5].GetParameter(j);
      }

      EventData->Draw("time_2thr3-time_2thr4:trail_2thr3-time_2thr3",ScanCut[i]+"max1>5","same");
      fp[5].Draw("same");
      c1.Update();
      //getchar();
     //fdibello - add boarder event - why do we need this, to initialiaze after the loop??
    // htwhex_border->Reset();

      // Central events (in slices)
      double prevslice=0;
      double mtotlow=totcutlow;
      totslice=0.05;
      double mtothigh=0;;
      double mtw[int((totcuthigh-totcutlow)/totslice)];
      double mtot[int((totcuthigh-totcutlow)/totslice)];
      double errtw[int((totcuthigh-totcutlow)/totslice)];
      double errtot[int((totcuthigh-totcutlow)/totslice)];
      double fitlow,fithigh;
      int j=0;
      double chi2[int((totcuthigh-totcutlow)/totslice)];
      int gtestpoints;
      while(mtothigh<totcuthigh){
	totslice=0.05;
	mtotlow=mtotlow+prevslice;
	for(int k=0;k<50;k++){
	  EventData->Draw("time_2thr3-time_2thr4>>hgaus",ScanCut[i]+" max1<5 "+Form(" trail_2thr3-time_2thr3>%f && trail_2thr3-time_2thr3<%f",mtotlow,mtotlow+totslice),"");
	  if(hgaus->GetEntries()<100){
	    totslice=totslice+0.01;
	  }
	  else{
	    break;
	  }
	}
	mtothigh=mtotlow+totslice;
	hgaus->Reset();
	EventData->Draw("time_2thr3-time_2thr4>>hgaus",ScanCut[i]+" max1<5 "+Form(" trail_2thr3-time_2thr3>%f && trail_2thr3-time_2thr3<%f",mtotlow,mtothigh),"");
	fitlow=hgaus->GetBinCenter(hgaus->GetMaximumBin())-0.5;
	fithigh=hgaus->GetBinCenter(hgaus->GetMaximumBin())+0.5;
	hgaus->Fit("fgaus","Q","",fitlow,fithigh);
	c1.Update();
	if(fgaus.GetNDF()>0){
	  chi2[j]=fgaus.GetChisquare()/double(fgaus.GetNDF());
	}
	else{
	  chi2[j]=-1;
	}
	mtw[j]=fgaus.GetParameter(1);
	errtw[j]=fgaus.GetParError(1);
	errtot[j]=0;
	mtot[j]=0.5*(mtothigh+mtotlow);
	//getchar();
	hgaus->Reset();
	prevslice=totslice;
	j++;
	gtestpoints=j;
      }
      double mtwplot[j];
      double mtotplot[j];
      double errtwplot[j];
      double errtotplot[j];
      double chi2plot[j];
      for(int k=0;k<j;k++){
	mtwplot[k]=mtw[k];
	mtotplot[k]=mtot[k];
	errtwplot[k]=errtw[k];
	errtotplot[k]=errtot[k];
	chi2plot[k]=chi2[k];
	cout << mtotplot[k] << " " << mtwplot[k] << endl;
      }
      gtest->Set(j);
      for(int k=0;k<j;k++){
	gtest->SetPoint(k,mtotplot[k],chi2plot[k]);
      }
      gtest->SetMarkerStyle(23);
      gtest->SetMarkerSize(2);
      cout << j << endl;
      Ccheck->cd(2);
      gtest->Draw("AP");
      Ccheck->Update();
      //getchar();
      c1.cd();
      g->Set(j);
      g->SetMarkerColor(4);
      g->SetMarkerStyle(23);
      g->SetLineColor(4);
      for(int k=0;k<j;k++){
	g->SetPoint(k,mtotplot[k],mtwplot[k]);
	g->SetPointError(k,errtotplot[k],errtwplot[k]);
      }
      g->Draw("APC");
      c1.Update();
      //getchar();
      g->GetXaxis()->SetRangeUser(totcutlow,totcuthigh);
      g->GetYaxis()->SetRangeUser(tofcutlow,tofcuthigh);
      g->Fit(&fp[0],"NQ","",totcutlow,totcuthigh);

      for(int j=1;j<6;j++){
	fp[j].SetParameter(j,0);
	for(int k=0;k<j;k++){
	  fp[j].SetParameter(k,fp[j-1].GetParameter(k));
	}
	g->Fit(&fp[j],"","",totcutlow,totcuthigh);
      }
      c1.Update();
      //getchar();
      for(int j=0;j<6;j++){
	TWhex[j]=fp[5].GetParameter(j);
	fp5.SetParameter(j,TWhex[j]);
      }
      htwhex->Reset();
      c1.Update();

      EventData->Draw("time_2thr3-time_2thr4:trail_2thr3-time_2thr3>>htwhex",ScanCut[i]+" max1<5 "+Form(" trail_2thr3-time_2thr3>%f && trail_2thr3-time_2thr3<%f",totcutlow,totcuthigh),"prof");
      EventData->Draw("time_2thr3-time_2thr4:trail_2thr3-time_2thr3>>+htwhex",ScanCut[i]+" max1>=5 "+Form(" trail_2thr3-time_2thr3>%f && trail_2thr3-time_2thr3<%f",totcutlow,totcuthigh),"prof");
      EventData->Draw(Form("time_2thr3-time_2thr4:trail_2thr3-time_2thr3>>h(100,%f,%f,100,%f,%f)",totcutlow,totcuthigh,tofcutlow,tofcuthigh),ScanCut[i]+"max1<5","same");
      htwhex->GetXaxis()->SetRangeUser(totcutlow,totcuthigh);
      htwhex->GetYaxis()->SetRangeUser(tofcutlow,tofcuthigh);
      fp[5].Draw("same");
      c1.Update();
      Ccheck->cd(3);
      //htwhex->Draw();
      g->Draw("AP");
      g->SetTitle("Time walk correction, DUT; TOT [ns]; TOF[ns]");
      h->SetMarkerSize(0.1);
      h->Draw("same");
      g->Draw("P");
      fp5.SetLineWidth(1);
      fp5.Draw("same");
      Ccheck->Update();
      gPad->Update();
      c1.cd();
      g->Draw("AP");
      g->SetTitle("Time walk correction, DUT; TOT [ns]; TOF[ns]");
      h->SetMarkerSize(0.1);
      h->Draw("same");
      g->Draw("P");
      fp5.SetLineWidth(1);
      fp5.Draw("same");
      c1.Update();
      //getchar();

    }

    if(doslices==0){
      htwhex->SetTitle("Time walk correction, DUT; TOT [ns]; TOF[ns]");
      htwhex->SetLineWidth(3);
      htwhex->GetXaxis()->SetLabelFont(42);
      htwhex->GetXaxis()->SetLabelSize(0.05);
      htwhex->GetXaxis()->SetTitleSize(0.05);
      htwhex->GetXaxis()->SetTitleOffset(0.94);
      htwhex->GetXaxis()->SetTitleFont(42);
      htwhex->GetYaxis()->SetLabelFont(42);
      htwhex->GetYaxis()->SetLabelSize(0.05);
      htwhex->GetYaxis()->SetTitleSize(0.05);
      htwhex->GetYaxis()->SetTitleOffset(0.95);
      htwhex->GetYaxis()->SetTitleFont(42);
      htwhex->SetStats(0);
      ptstats_hex->Clear();
      ptstats_hex_LaTex = ptstats_hex->AddText(Form("Entries = %5.0f   ",htwhex->GetEntries()));
      ptstats_hex->SetOptStat(0000);
      htwhex->GetListOfFunctions()->Add(ptstats_hex);
      ptstats_hex->SetParent(htwhex);

      pinfo_hex->Clear();
      pinfo_hex_LaTex = pinfo_hex->AddText(Form("HV = %3.0f V ",HV[i]));
      pinfo_hex_LaTex = pinfo_hex->AddText(Form("Threshold = %3.0f e^{_ } ",Thr[i]));
      pinfo_hex->Draw();
      pinfo_hex->SetOptStat(0000);
      htwhex->GetListOfFunctions()->Add(pinfo_hex);
      pinfo_hex->SetParent(htwhex);
    }
    else{
      h->SetTitle("Time walk correction, DUT; TOT [ns]; TOF[ns]");
      h->SetLineWidth(3);
      h->GetXaxis()->SetLabelFont(42);
      h->GetXaxis()->SetLabelSize(0.05);
      h->GetXaxis()->SetTitleSize(0.05);
      h->GetXaxis()->SetTitleOffset(0.94);
      h->GetXaxis()->SetTitleFont(42);
      h->GetYaxis()->SetLabelFont(42);
      h->GetYaxis()->SetLabelSize(0.05);
      h->GetYaxis()->SetTitleSize(0.05);
      h->GetYaxis()->SetTitleOffset(0.95);
      h->GetYaxis()->SetTitleFont(42);
      h->SetStats(0);
      ptstats_hex->Clear();
      ptstats_hex_LaTex = ptstats_hex->AddText(Form("Entries = %5.0f   ",h->GetEntries()));
      ptstats_hex->SetOptStat(0000);
      h->GetListOfFunctions()->Add(ptstats_hex);
      ptstats_hex->SetParent(g);

      pinfo_hex->Clear();
      pinfo_hex_LaTex = pinfo_hex->AddText(Form("HV = %3.0f V ",HV[i]));
      pinfo_hex_LaTex = pinfo_hex->AddText(Form("Threshold = %3.0f e^{_ } ",Thr[i]));
      pinfo_hex->Draw();
      pinfo_hex->SetOptStat(0000);
      h->GetListOfFunctions()->Add(pinfo_hex);
      pinfo_hex->SetParent(g);
    }

    c1.Update();
    cmd.clear();
    cmd.str("");
    cmd << "Run" << ScanList[i] << "_TWhexa.png";
    c1.SaveAs(cmd.str().c_str());
    //sleep(1);
    //getchar();

    cmd.clear();
    cmd.str("");

    cmd << "time_2thr3-time_2thr4-(";
    for(int j=0;j<5;j++){
      cmd << TWhex[j] << "*pow(trail_2thr3-time_2thr3," << j << ")+";
    }
    cmd << TWhex[5] << "*pow(trail_2thr3-time_2thr3,5)):max4>>htwLGAD";
    //cout << cmd.str() << endl;

    EventData->Draw(cmd.str().c_str(),ScanCut[i]+"max1<5","prof");

    htwLGAD->GetXaxis()->SetRangeUser(LGADcutlow,LGADcuthigh);
    htwLGAD->GetYaxis()->SetRangeUser(-1,1);
    htwLGAD->Fit(&fp[0],"QN","",LGADcutlow,LGADcuthigh);
    for(int j=1;j<6;j++){
      fp[j].SetParameter(j,0);
      for(int k=0;k<j;k++){
	fp[j].SetParameter(k,fp[j-1].GetParameter(k));
      }
      htwLGAD->Fit(&fp[j],"NQ","",LGADcutlow,LGADcuthigh);
    }
    EventData->Draw(Form("time_2thr3-time_2thr4-(%f+%f*(trail_2thr3-time_2thr3)+%f*pow(trail_2thr3-time_2thr3,2)+%f*pow(trail_2thr3-time_2thr3,3)+%f*pow(trail_2thr3-time_2thr3,4)+%f*pow(trail_2thr3-time_2thr3,5)):max4",TWhex[0],TWhex[1],TWhex[2],TWhex[3],TWhex[4],TWhex[5]),ScanCut[i],"same");
    fp[3].Draw("same");
    htwLGAD->SetTitle("Time walk correction, LGAD; TOT [ns]; TOF[ns]");
    htwLGAD->SetLineWidth(3);
    htwLGAD->GetXaxis()->SetLabelFont(42);
    htwLGAD->GetXaxis()->SetLabelSize(0.05);
    htwLGAD->GetXaxis()->SetTitleSize(0.05);
    htwLGAD->GetXaxis()->SetTitleOffset(0.94);
    htwLGAD->GetXaxis()->SetTitleFont(42);
    htwLGAD->GetYaxis()->SetLabelFont(42);
    htwLGAD->GetYaxis()->SetLabelSize(0.05);
    htwLGAD->GetYaxis()->SetTitleSize(0.05);
    htwLGAD->GetYaxis()->SetTitleOffset(0.95);
    htwLGAD->GetYaxis()->SetTitleFont(42);
    htwLGAD->SetStats(0);

    ptstats_LGAD->Clear();
    ptstats_LGAD_LaTex = ptstats_LGAD->AddText(Form("Entries = %5.0f   ",htwLGAD->GetEntries()));
    ptstats_LGAD->SetOptStat(0000);

    htwLGAD->GetListOfFunctions()->Add(ptstats_LGAD);
    ptstats_LGAD->SetParent(htwLGAD);

    c1.Update();
    cmd.clear();
    cmd.str("");
    cmd << "Run" << ScanList[i] << "_TWLGAD.png";
    c1.SaveAs(cmd.str().c_str());
    htwLGAD->GetListOfFunctions()->Remove(ptstats_LGAD);
    htwLGAD->Reset();
    //sleep(1);
    //getchar();

    for(int j=0;j<3;j++){
      TWLGAD[j]=fp[2].GetParameter(j);
    }

    // Adding to the TOF hinstogram the central events
    ftof.SetLineStyle(1);

    cmd.clear();
    cmd.str("");
    cmd << "time_2thr3-time_2thr4-(";
    for(int j=0;j<5;j++){
      cmd << TWhex[j] << "*pow(trail_2thr3-time_2thr3," << j << ")+";
    }
    cmd << TWhex[5] << "*pow(trail_2thr3-time_2thr3,5))-(";
    for(int j=0;j<3;j++){
      cmd << TWLGAD[j] << "*pow(max4," << j << ")+";
    }
    ostringstream cmd2D;
    cmd2D << cmd.str() ;
    cmd << TWLGAD[3] << "*pow(max4,3))>>hTOF";
    cmd2D << TWLGAD[3] << "*pow(max4,3)):ntrig>>hTOF2D";
    //cout << cmd.str() << endl;
    if(DoBorder!=2)EventData->Draw(cmd.str().c_str(),ScanCut[i]+"max1<5","");
    if(DoBorder!=2)EventData->Draw(cmd2D.str().c_str(),ScanCut[i]+"max1<5","");

    // Adding to the TOF hinstogram the border events
    if(DoBorder>0){
      cmd.clear();
      cmd.str("");
      cmd << "time_2thr3-time_2thr4-(";
      for(int j=0;j<5;j++){
	cmd << TWhexSmall[j] << "*pow(trail_2thr3-time_2thr3," << j << ")+";
      }
      cmd << TWhexSmall[5] << "*pow(trail_2thr3-time_2thr3,5))-(";
      for(int j=0;j<3;j++){
	cmd << TWLGAD[j] << "*pow(max4," << j << ")+";
      }
      cmd2D.clear();
      cmd2D.str("");
      cmd2D << cmd.str() ;
      cmd << TWLGAD[3] << "*pow(max4,3))>>+hTOF";
      cmd2D << TWLGAD[3] << "*pow(max4,3)):ntrig>>+hTOF2D";

      cout << cmd.str() << endl;
      cout << cmd2D.str() << endl;
      EventData->Draw(cmd.str().c_str(),ScanCut[i]+"max1>5","");
      EventData->Draw(cmd2D.str().c_str(),ScanCut[i]+"max1>5","");
    }

    hTOF->SetTitle("Time of flight (time walk corrected); TOF [ns]; Entries");
    hTOF->SetLineWidth(3);
    hTOF->GetXaxis()->SetLabelFont(42);
    hTOF->GetXaxis()->SetLabelSize(0.05);
    hTOF->GetXaxis()->SetTitleSize(0.05);
    hTOF->GetXaxis()->SetTitleOffset(0.94);
    hTOF->GetXaxis()->SetTitleFont(42);
    hTOF->GetYaxis()->SetLabelFont(42);
    hTOF->GetYaxis()->SetLabelSize(0.05);
    hTOF->GetYaxis()->SetTitleSize(0.05);
    hTOF->GetYaxis()->SetTitleOffset(0.95);
    hTOF->GetYaxis()->SetTitleFont(42);
    hTOF->SetStats(0);

    // Calculation FWHM
    double FWHM;
    double errFWHM=hTOF->GetBinCenter(2)-hTOF->GetBinCenter(1);
    double hmax=hTOF->GetMaximum();
    cout << "Maximum = " << hmax << endl;
    int binmax=hTOF->GetMaximumBin();
    cout << "Max position = " << hTOF->GetBinCenter(hTOF->GetMaximumBin()) << endl;;
    int islow=1;
    int ishigh=0;
    int hnbins=hTOF->GetXaxis()->GetNbins();

    double highedge,lowedge;

    for(int j=0;j<hnbins;j++){
      if(hTOF->GetBinContent(j)>hmax/2. && islow==1){
	FWHM=hTOF->GetBinCenter(j);
	lowedge=hTOF->GetBinCenter(j);
      	islow=0;
	cout << "Low edge = " << FWHM << endl;
      }
      if(j>binmax && ishigh==0 && islow==0){
      	ishigh=1;
      }
      if(hTOF->GetBinContent(j)<hmax/2. && ishigh==1 && islow==0){
	highedge=hTOF->GetBinCenter(j);
      	FWHM=hTOF->GetBinCenter(j)-FWHM;
	FWHM=highedge-lowedge;
      	ishigh=0;
	islow=1;
	cout << "High edge = " << hTOF->GetBinCenter(j) << endl;
      }
    }
    cout << "Run " << ScanList[i] <<  endl;
    cout << "FWHM=" << 1000.*FWHM << endl;
    cout << "FWHM/2.355=" << 1000.*FWHM/2.355 << endl;
    cout << "FWHM/2.355 w/o LGAD =" << 1000.*sqrt(pow(FWHM/2.355,2)-pow(0.05,2)) << endl;

    hTOF->Fit("ftof","Q","",-0.4,0.4);
    double sigmatemp;
    sigmatemp=ftof.GetParameter(2);
    cout << sigmatemp << endl;
    hTOF->Fit("ftof","Q","",-sigmatemp,sigmatemp);

    hTOF->Fit("ftof","Q","",lowedge,highedge);
    hTOF->Fit("ftof","Q","",-0.25,0.1);

    cout << "SigmaFit=" << 1000.*ftof.GetParameter(2) << endl;
    cout << "SigmaFit w/o LGAD =" << 1000.*sqrt(pow(ftof.GetParameter(2),2)-pow(0.05,2)) << endl;

    // Calculation tails
    double residual=0;

    for(int j=0;j<hnbins;j++){
      if(hTOF->GetBinCenter(j)<-0.1 || hTOF->GetBinCenter(j)>0.1){
	residual=residual+(hTOF->GetBinContent(j)-ftof(hTOF->GetBinCenter(j)));
      }
    }
    cout << "Residual = " << residual << endl;
    double tails= 0;
    if(hTOF->GetEntries() > 0)tails = residual/double(hTOF->GetEntries());
    ftof.SetLineStyle(7);
    ftof.Draw("same");
    ptstats_TOF->Clear();

    ptstats_TOF_LaTex = ptstats_TOF->AddText(Form("Entries = %5.0f   ",hTOF->GetEntries()));
    ptstats_TOF_LaTex = ptstats_TOF->AddText(Form("Std Dev = %1.3f   ",hTOF->GetStdDev()));
    ptstats_TOF_LaTex = ptstats_TOF->AddText(Form("FWHM = %0.3f  #pm  %0.3f", FWHM, errFWHM));
    ptstats_TOF_LaTex = ptstats_TOF->AddText(Form("Constant = %3.0f  #pm  %1.0f",ftof.GetParameter(0), ftof.GetParError(0)));
    ptstats_TOF_LaTex = ptstats_TOF->AddText(Form("Mean = %0.3f  #pm  %0.3f",ftof.GetParameter(1), ftof.GetParError(1)));
    ptstats_TOF_LaTex = ptstats_TOF->AddText(Form("Sigma = %0.3f  #pm  %0.3f",ftof.GetParameter(2), ftof.GetParError(2)));
    ptstats_TOF_LaTex = ptstats_TOF->AddText(Form("Tails = %1.1f %%",tails*100));
    ptstats_TOF_LaTex = ptstats_TOF->AddText(Form(" #chi^{2}/NDF = %3.2f",ftof.GetChisquare()/ftof.GetNDF()));

    ptstats_TOF->Draw();
    ptstats_TOF->SetOptStat(0000);

    hTOF->GetListOfFunctions()->Add(ptstats_TOF);
    ptstats_TOF->SetParent(hTOF);

    pinfo_TOF->Clear();

    TLatex* write1 = new TLatex();
    write1->SetTextFont(62);
    //write1->SetTextSize(0.035);
    write1->SetNDC();
    write1->DrawLatex(0.2,0.7,Form("HV = %3.0f V ",HV[i]));
    write1->DrawLatex(0.2,0.6,Form("Th. = %3.0f e^{_ } ",Thr[i]));

//    pinfo_TOF_LaTex = pinfo_TOF->AddText(Form("HV = %3.0f V ",HV[i]));
//    pinfo_TOF_LaTex = pinfo_TOF->AddText(Form("Threshold = %3.0f e^{_ } ",Thr[i]));
//    pinfo_TOF->Draw();
    pinfo_TOF->SetOptStat(0000);
   // hTOF->GetListOfFunctions()->Add(pinfo_TOF);
   // pinfo_TOF->SetParent(hTOF);

    Sigma[i]=sqrt(pow(1000*ftof.GetParameter(2),2)-pow(50,2));
    SigmaErr[i]=1000*ftof.GetParError(2);

    c1.Update();

    Ccheck->cd(4);
    hTOF->Draw();
    ftof.Draw("same");
    Ccheck->Update();
    c1.cd();
    //ftof.SetLineStyle(1);
    cmd.clear();
    cmd.str("");
    if(DoBorder==1){
      cmd << "Run" << ScanList[i] << "_TOF.png";
    }
    else{
      cmd << "Run" << ScanList[i] << "_TOF_BorderCut.png";
    }
    c1.SaveAs(cmd.str().c_str());

    cmd.clear();
    cmd.str("");
    if(DoBorder==1){
      cmd << "Control_Run" << ScanList[i] << "_TOF.png";
    }
    else{
      cmd << "Control_Run" << ScanList[i] << "_TOF_BorderCut.png";
    }
    //Ccheck->cd();
    Ccheck->Update();
    cout << "Okay here"  <<  endl;
    //getchar();
    Ccheck->Update();
    gPad->Update();
    //gPad->SaveAs(cmd.str().c_str());
    Ccheck->SaveAs(cmd.str().c_str());
    cmd.clear();
    cmd.str("");
    if(DoBorder==1){
      cmd << "Control_Run" << ScanList[i] << "_TOF.png";
    }
    else{
      cmd << "Control_Run" << ScanList[i] << "_TOF_BorderCut.png";
    }
    Ccheck->SaveAs(cmd.str().c_str());
    //sleep(1);
    //getchar();

    //fdibello need to write this here - why are you removing these?

   cout<<"CLONING"<<endl;
   hTOF_new = (TH1D*)hTOF->Clone("hTOF_new");
   hTOT_new = (TH1D*)hTOT->Clone("hTOT_new");
   hTOF2D_new = (TH2D*)hTOF2D->Clone("hTOF2D_new");

    hTOF->GetListOfFunctions()->Remove(ptstats_TOF);
    hTOF->GetListOfFunctions()->Remove(pinfo_TOF);
    hTOF->Reset();

    hTOT->GetListOfFunctions()->Remove(ptstats_TOT);
    hTOT->GetListOfFunctions()->Remove(pinfo_TOT);
    hTOT->Reset();
    if(doslices==0){
      htwhex->GetListOfFunctions()->Remove(ptstats_hex);
      htwhex->GetListOfFunctions()->Remove(pinfo_hex);
    }
    else{
      h->GetListOfFunctions()->Remove(ptstats_hex);
      h->GetListOfFunctions()->Remove(pinfo_hex);
    }
    htwhex->Reset();

  }

  TGraphErrors gs(nscan,Thr,Sigma,ThrErr,SigmaErr);
  gs.SetTitle("Time Resolution vs Discrimination Threshold; Threshold [e^{_ }]; Time resolution [ps]");
  gs.SetLineWidth(3);
  gs.SetMarkerStyle(20);
  gs.SetMarkerSize(2);
  c1.SetGridx();
  c1.SetGridy();
  gs.GetXaxis()->SetRangeUser(950,990);
  gs.GetYaxis()->SetRangeUser(0,100);
  gs.GetXaxis()->SetLabelFont(42);
  gs.GetXaxis()->SetLabelSize(0.05);
  gs.GetXaxis()->SetTitleSize(0.05);
  gs.GetXaxis()->SetTitleOffset(0.94);
  gs.GetXaxis()->SetTitleFont(42);
  gs.GetYaxis()->SetLabelFont(42);
  gs.GetYaxis()->SetLabelSize(0.05);
  gs.GetYaxis()->SetTitleSize(0.05);
  gs.GetYaxis()->SetTitleOffset(0.95);
  gs.GetYaxis()->SetTitleFont(42);
  gs.Draw("AP");
  c1.Update();
  cmd.clear();
  cmd.str("");
  if(DoBorder==1){
    cmd << "Scan_" << ScanList[0] << "_" << ScanList[nscan-1] << "_TOF.png";
  }
  else{
    cmd << "Scan_" << ScanList[0] << "_" << ScanList[nscan-1] << "_TOF_BorderCut.png";
  }
  c1.SaveAs(cmd.str().c_str());

  // Formatted output:

  cout << "Threshold array" << endl;
  cout << "{";
  for(int i=0;i<nscan-1;i++){
    cout << Thr[i] << ",";
  }
  cout << Thr[nscan-1] << "}" << endl << endl;

  cout << "Threshold Error array" << endl;
  cout << "{";
  for(int i=0;i<nscan-1;i++){
    cout << ThrErr[i] << ",";
  }
  cout << ThrErr[nscan-1] << "}" << endl << endl;

  cout << "Sigma array" << endl;
  cout << "{";
  for(int i=0;i<nscan-1;i++){
    cout << Sigma[i] << ",";
  }
  cout << Sigma[nscan-1] << "}" << endl << endl;

  cout << "Sigma Error array" << endl;
  cout << "{";
  for(int i=0;i<nscan-1;i++){
    cout << SigmaErr[i] << ",";
  }
  cout << SigmaErr[nscan-1] << "}" << endl << endl;
   TFile *Output = new TFile("Francesco_plots.root","RECREATE");
//   hTOF_new->Write();
//   hTOF2D_new->Write();
//   ftof.Write();
//   hTOT_new->Write();
//   htwhex_border->Write();
//   htwhex->Write();
//   g->SetName("time_walk");
//   g->Write();
//   htwhex_border_save_before->Write();
   Output->Close();
}
